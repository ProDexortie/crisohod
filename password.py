import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QLineEdit, QVBoxLayout, QWidget, QLabel, QMessageBox
from selection_window import Ui_MainWindow
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QFont
import bcrypt

import os
from PyQt5.QtGui import QIcon
from all_data import main_folder_path
os.chdir(main_folder_path)


class SecondWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)  # Инициализируем дизайн
        self.setWindowTitle("Крысоход")


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Окно входа")
        self.setWindowIcon(QIcon('icons/lock.png'))
        self.hello_text = QtWidgets.QLabel(self)
        self.hello_text.setText("Введите пароль")
        # Установка шрифта
        font = QFont()  # Создаем объект QFont
        font.setPointSize(23)
        self.hello_text.setFont(font)


        self.password_input = QLineEdit(self)
        self.password_input.setEchoMode(QLineEdit.Password)
        self.login_button = QPushButton('Войти', self)
        self.login_button.clicked.connect(self.check_password)

        layout = QVBoxLayout()
        layout.addWidget(self.hello_text)
        layout.addWidget(self.password_input)
        layout.addWidget(self.login_button)

        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)

    def check_password(self):
        # Чтение хеша из файла
        from all_data import password_path

        with open(password_path, "rb") as f:
            saved_hash = f.read()

        # Проверка введенного пароля
        if bcrypt.checkpw(self.password_input.text().encode(), saved_hash):
            self.second_window = SecondWindow()
            self.second_window.show()
            self.hide()
        else:
            QMessageBox.warning(self, "Ошибка", "Неверный пароль")

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Return or event.key() == QtCore.Qt.Key_Enter:
            self.check_password()


# Создаем экземпляр QApplication
app = QApplication(sys.argv)
main_window = MainWindow()
main_window.show()
# Запускаем основной цикл приложения
sys.exit(app.exec_())
