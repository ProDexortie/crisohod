#pip install opencv-python
#pip install PyQt5
#pip install bcrypt
#pip install ultralytics

#Необходимо установить:
#K-Lite Codec Pack


import os.path


image_horizontal_lb_width = 899#Ширина всех image_horizontal(оказывается она не вычисляется, находясь в verticalLayout))


main_folder_path = os.path.dirname(os.path.abspath(__file__)) # Путь к папке c проектом


#--------------------МЕНЯТЬ ТОЛЬКО ПУТЬ К ПАПКЕ С ПРОЕКТОМ-------------------------------#
#----------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------#

password_path = main_folder_path + r"/data/password_hash.txt" #путь к паролю(password.py)

video_folder_path = main_folder_path + r'/data/video'#Путь к папке видео(selection_window.py)

processed_video_folder_path = main_folder_path + r"/data/video/processed_video"

paw_data_path = main_folder_path + r"/data/paw_data.txt"#Путь к файлу для вывода информации(selection_window.py)


lb_photos_path = main_folder_path + r"/data/processed_photos_of_paws/lb" #Путь к фото(selection_window.py)
lf_photos_path = main_folder_path + r"/data/processed_photos_of_paws/lf" #Путь к фото(selection_window.py)
rb_photos_path = main_folder_path + r"/data/processed_photos_of_paws/rb" #Путь к фото(selection_window.py)
rf_photos_path = main_folder_path + r"/data/processed_photos_of_paws/rf" #Путь к фото(selection_window.py)


duration_of_click_path = main_folder_path + r"/data/paws_on_corridor/duration_of_click.txt" #Путь к duration_of_click.txt

photo_of_corridor_path = main_folder_path + r"/data/paws_on_corridor/photo_of_corridor.png" #Путь к фото каридора с лапами

model_path = main_folder_path + r"/data/model.pt" #Путь к обученной моделе

