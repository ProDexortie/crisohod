from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtCore import QUrl
import os



class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1081, 781)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(1081, 781))
        MainWindow.setMaximumSize(QtCore.QSize(1081, 781))
        MainWindow.setStyleSheet("background-color: rgb(91, 91, 91);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 1081, 241))
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        font.setKerning(True)
        self.tabWidget.setFont(font)
        self.tabWidget.setStyleSheet("QTabWidget::pane { /* Удаление границ вокруг содержимого вкладок */\n"
"    border: 0;\n"
"}\n"
"\n"
"QTabBar::tab { /* Стилизация вкладок */\n"
"    background: rgb(255, 130, 20);\n"
"    color: rgb(255, 255, 255);\n"
"    border: 1px solid rgb(0, 0, 0);\n"
"    padding: 5px;\n"
"    border-radius: 7px;\n"
"}\n"
"\n"
"QTabBar::tab:selected { /* Стилизация выбранной вкладки */\n"
"    background: #ffffff;\n"
"    color: rgb(0, 0, 0);\n"
"    border-color: #ffffff;\n"
"    border-radius:1px;\n"
"\n"
"}\n"
"")
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.hallway_photo = QtWidgets.QLabel(self.tab)
        self.hallway_photo.setGeometry(QtCore.QRect(0, 0, 1081, 131))
        self.hallway_photo.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.hallway_photo.setText("")
        self.hallway_photo.setObjectName("hallway_photo")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.tab)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(170, 130, 911, 81))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.verticalLayoutWidget.setFont(font)
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.image_horizontal_lf = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.image_horizontal_lf.setFont(font)
        self.image_horizontal_lf.setStyleSheet("background-color: rgb(135, 135, 135);")
        self.image_horizontal_lf.setText("")
        self.image_horizontal_lf.setObjectName("image_horizontal_lf")
        self.verticalLayout.addWidget(self.image_horizontal_lf)
        self.image_horizontal_rf = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.image_horizontal_rf.setFont(font)
        self.image_horizontal_rf.setStyleSheet("background-color: rgb(206, 206, 206);")
        self.image_horizontal_rf.setText("")
        self.image_horizontal_rf.setObjectName("image_horizontal_rf")
        self.verticalLayout.addWidget(self.image_horizontal_rf)
        self.image_horizontal_lb = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.image_horizontal_lb.setFont(font)
        self.image_horizontal_lb.setStyleSheet("background-color: rgb(135, 135, 135);")
        self.image_horizontal_lb.setText("")
        self.image_horizontal_lb.setObjectName("image_horizontal_lb")
        self.verticalLayout.addWidget(self.image_horizontal_lb)
        self.image_horizontal_rb = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.image_horizontal_rb.setFont(font)
        self.image_horizontal_rb.setStyleSheet("background-color: rgb(206, 206, 206);")
        self.image_horizontal_rb.setText("")
        self.image_horizontal_rb.setObjectName("image_horizontal_rb")
        self.verticalLayout.addWidget(self.image_horizontal_rb)
        self.verticalLayoutWidget_2 = QtWidgets.QWidget(self.tab)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(0, 130, 171, 81))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.verticalLayoutWidget_2.setFont(font)
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontal_lf = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.horizontal_lf.setFont(font)
        self.horizontal_lf.setStyleSheet("background-color: rgb(135, 135, 135);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"QLabel {\n"
"    color: white;\n"
"    background-color: black;\n"
"    padding: 1px; /* Минимальное расстояние текста от краев виджета */\n"
"}\n"
"")
        self.horizontal_lf.setObjectName("horizontal_lf")
        self.verticalLayout_2.addWidget(self.horizontal_lf)
        self.horizontal_rf = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.horizontal_rf.setFont(font)
        self.horizontal_rf.setStyleSheet("background-color: rgb(206, 206, 206);")
        self.horizontal_rf.setObjectName("horizontal_rf")
        self.verticalLayout_2.addWidget(self.horizontal_rf)
        self.horizontal_lb = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.horizontal_lb.setFont(font)
        self.horizontal_lb.setStyleSheet("background-color: rgb(135, 135, 135);\n"
"color: rgb(255, 255, 255);\n"
"")
        self.horizontal_lb.setObjectName("horizontal_lb")
        self.verticalLayout_2.addWidget(self.horizontal_lb)
        self.horizontal_rb = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.horizontal_rb.setFont(font)
        self.horizontal_rb.setStyleSheet("background-color: rgb(206, 206, 206);")
        self.horizontal_rb.setObjectName("horizontal_rb")
        self.verticalLayout_2.addWidget(self.horizontal_rb)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.horizontalSlider = QtWidgets.QSlider(self.tab_2)
        self.horizontalSlider.setGeometry(QtCore.QRect(0, 180, 1071, 31))
        self.horizontalSlider.setStyleSheet("QSlider::groove:horizontal {\n"
"    border: 1px solid #999999; /* Цвет границы дорожки */\n"
"    height: 9px; /* Высота дорожки */\n"
"    background: #e0e0e0; /* Цвет фона дорожки */\n"
"    margin: 2px 0; /* Отступы вокруг дорожки */\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: rgb(255, 130, 20); /* Цвет ползунка */\n"
"    border: 1px solid #5c5c5c; /* Цвет границы ползунка */\n"
"    width: 12px; /* Ширина ползунка */\n"
"    margin: -28px 0; /* Вертикальный отступ ползунка от дорожки */\n"
"    border-radius: 20px; /* Закругление углов ползунка */\n"
"}\n"
"\n"
"QSlider::sub-page:horizontal {\n"
"    background: rgb(0, 161, 0); /* Зеленый цвет для заполненной части слайдера */\n"
"}\n"
"\n"
"QSlider::add-page:horizontal {\n"
"    background: #d3d3d3; /* Серый цвет для незаполненной части слайдера */\n"
"}\n"
"")
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setObjectName("horizontalSlider")

        self.videoWiget = QVideoWidget(self.tab_2)
        self.videoWiget.setGeometry(QtCore.QRect(0, 0, 1071, 171))
        self.videoWiget.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.videoWiget.setObjectName("videoWiget")

        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        self.mediaPlayer.setVideoOutput(self.videoWiget)
        self.mediaPlayer.setMedia(QMediaContent(QUrl.fromLocalFile("video/test video.mp4")))




        self.tabWidget.addTab(self.tab_2, "")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(10, 260, 551, 80))
        self.widget.setStyleSheet("\n"
"border-radius: 9px;\n"
"background-color: rgb(113, 113, 113)")
        self.widget.setObjectName("widget")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.widget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 0, 531, 80))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.checkBox_lf = QtWidgets.QCheckBox(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.checkBox_lf.setFont(font)
        self.checkBox_lf.setStyleSheet("color: rgb(255, 255, 255);")
        self.checkBox_lf.setObjectName("checkBox_lf")
        self.horizontalLayout.addWidget(self.checkBox_lf)
        self.checkBox_rf = QtWidgets.QCheckBox(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.checkBox_rf.setFont(font)
        self.checkBox_rf.setStyleSheet("color: rgb(255, 255, 255);")
        self.checkBox_rf.setObjectName("checkBox_rf")
        self.horizontalLayout.addWidget(self.checkBox_rf)
        self.checkBox_lb = QtWidgets.QCheckBox(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.checkBox_lb.setFont(font)
        self.checkBox_lb.setStyleSheet("color: rgb(255, 255, 255);")
        self.checkBox_lb.setObjectName("checkBox_lb")
        self.horizontalLayout.addWidget(self.checkBox_lb)
        self.checkBox_rb = QtWidgets.QCheckBox(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.checkBox_rb.setFont(font)
        self.checkBox_rb.setStyleSheet("color: rgb(255, 255, 255);")
        self.checkBox_rb.setObjectName("checkBox_rb")
        self.horizontalLayout.addWidget(self.checkBox_rb)

        # Настройка checkBox сигналов
        self.checkBox_lf.stateChanged.connect(self.update_list_widget)
        self.checkBox_rf.stateChanged.connect(self.update_list_widget)
        self.checkBox_lb.stateChanged.connect(self.update_list_widget)
        self.checkBox_rb.stateChanged.connect(self.update_list_widget)


        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(10, 360, 551, 81))
        self.listWidget.setStyleSheet("border-radius: 9px;\n"
"background-color: rgb(0, 0, 0);\n"
"border: 1px solid rgb(255, 130, 20);")
        self.listWidget.setFlow(QtWidgets.QListView.LeftToRight)
        self.listWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.listWidget.setObjectName("listWidget")

        self.widget_3 = QtWidgets.QWidget(self.centralwidget)
        self.widget_3.setGeometry(QtCore.QRect(580, 260, 491, 491))
        self.widget_3.setStyleSheet("\n"
"border-radius: 9px;\n"
"background-color: rgb(113, 113, 113)")
        self.widget_3.setObjectName("widget_3")
        self.label_full_image = QtWidgets.QLabel(self.widget_3)
        self.label_full_image.setGeometry(QtCore.QRect(10, 10, 471, 471))
        self.label_full_image.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_full_image.setText("")
        self.label_full_image.setObjectName("label_full_image")
        self.widget_4 = QtWidgets.QWidget(self.centralwidget)
        self.widget_4.setGeometry(QtCore.QRect(10, 560, 551, 191))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_4.sizePolicy().hasHeightForWidth())
        self.widget_4.setSizePolicy(sizePolicy)
        self.widget_4.setStyleSheet("\n"
"border-radius: 9px;\n"
"background-color: rgb(113, 113, 113)")
        self.widget_4.setObjectName("widget_4")
        self.textBrowser = QtWidgets.QTextBrowser(self.widget_4)
        self.textBrowser.setGeometry(QtCore.QRect(10, 10, 531, 171))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.textBrowser.setFont(font)
        self.textBrowser.setStyleSheet("background-color: rgb(211, 211, 211);\n"
"color: rgb(0, 0, 0);")
        self.textBrowser.setObjectName("textBrowser")
        self.widget_3.raise_()
        self.tabWidget.raise_()
        self.widget.raise_()
        self.listWidget.raise_()
        self.widget_4.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1081, 23))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.menubar.setFont(font)
        self.menubar.setStyleSheet("border: 1px solid #ffffff;\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(240, 240, 240);")
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        self.menu_2 = QtWidgets.QMenu(self.menubar)
        self.menu_2.setObjectName("menu_2")
        self.menu_3 = QtWidgets.QMenu(self.menubar)
        self.menu_3.setObjectName("menu_3")
        MainWindow.setMenuBar(self.menubar)
        self.action_5 = QtWidgets.QAction(MainWindow)
        self.action_5.setObjectName("action_5")
        self.action_6 = QtWidgets.QAction(MainWindow)
        self.action_6.setObjectName("action_6")
        self.action_7 = QtWidgets.QAction(MainWindow)
        self.action_7.setObjectName("action_7")
        self.action_8 = QtWidgets.QAction(MainWindow)
        self.action_8.setObjectName("action_8")
        self.action_9 = QtWidgets.QAction(MainWindow)
        self.action_9.setObjectName("action_9")
        self.action_11 = QtWidgets.QAction(MainWindow)
        self.action_11.setObjectName("action_11")
        self.menu.addAction(self.action_5)
        self.menu.addAction(self.action_6)
        self.menu_2.addAction(self.action_8)
        self.menu_2.addAction(self.action_9)
        self.menu_2.addSeparator()
        self.menu_2.addAction(self.action_7)
        self.menu_2.addAction(self.action_11)
        self.menubar.addAction(self.menu_2.menuAction())
        self.menubar.addAction(self.menu.menuAction())
        self.menubar.addAction(self.menu_3.menuAction())

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.horizontal_lf.setText(_translate("MainWindow", "    Левая передняя"))
        self.horizontal_rf.setText(_translate("MainWindow", "    Правая передняя"))
        self.horizontal_lb.setText(_translate("MainWindow", "    Левая задняя"))
        self.horizontal_rb.setText(_translate("MainWindow", "    Правая задняя"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "Результаты"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "Видео"))
        self.checkBox_lf.setText(_translate("MainWindow", "Левая передняя"))
        self.checkBox_rf.setText(_translate("MainWindow", "Правая передняя"))
        self.checkBox_lb.setText(_translate("MainWindow", "Левая задняя"))
        self.checkBox_rb.setText(_translate("MainWindow", "Правая задняя"))
        self.textBrowser.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Растояниие между пальцами 1-2: 2мм</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Растояниие между пальцами 2-3: 3мм</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Растояниие между пальцами 3-4: 5мм</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Растояниие между пальцами 4-5: 3мм</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;\"><br /></p></body></html>"))
        self.menu.setTitle(_translate("MainWindow", "Настройки"))
        self.menu_2.setTitle(_translate("MainWindow", "Файл"))
        self.menu_3.setTitle(_translate("MainWindow", "Помощь"))
        self.action_5.setText(_translate("MainWindow", "Изменить пароль"))
        self.action_6.setText(_translate("MainWindow", "Изменить камеру"))
        self.action_7.setText(_translate("MainWindow", "Сохранить файл"))
        self.action_8.setText(_translate("MainWindow", "Нойвое видео"))
        self.action_9.setText(_translate("MainWindow", "Открыть файл"))
        self.action_11.setText(_translate("MainWindow", "Сохранить как..."))


    def update_list_widget(self):
            self.listWidget.clear()  # Очистка предыдущих элементов
            folders = {
                    'data/processed_photos_of_paws/lf': self.checkBox_lf.isChecked(),
                    'data/processed_photos_of_paws/rf': self.checkBox_rf.isChecked(),
                    'data/processed_photos_of_paws/lb': self.checkBox_lb.isChecked(),
                    'data/processed_photos_of_paws/rb': self.checkBox_rb.isChecked()
            }
            for folder, checked in folders.items():
                    if checked:
                            self.add_images_from_folder(folder)

    def add_images_from_folder(self, folder_path):
            valid_extensions = ['.jpg', '.jpeg', '.png', '.bmp', '.gif']
            self.listWidget.setIconSize(QtCore.QSize(126, 126))  # Установка размера иконки для всех элементов
            for filename in os.listdir(folder_path):
                    if any(filename.endswith(ext) for ext in valid_extensions):
                            file_path = os.path.join(folder_path, filename)
                            icon = QtGui.QIcon(file_path)
                            item = QtWidgets.QListWidgetItem(icon, "")
                            self.listWidget.addItem(item)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
