import sys
import cv2
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread, QTimer
from PyQt5.QtGui import QImage, QPixmap, QIcon
from PyQt5.QtWidgets import QDialog, QApplication, QMainWindow
import os
import numpy as np
from all_data import main_folder_path
os.chdir(main_folder_path)

class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(QImage)

    def __init__(self):
        super().__init__()
        self.cap = None
        self.running = False

    def run(self):
        self.cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # Используем DirectShow
        if not self.cap.isOpened():
            print("Error: Could not open video device.")
            return
        self.running = True
        while self.running:
            ret, cv_img = self.cap.read()
            if not ret:
                print("Error: Could not read frame.")
                break
            rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
            h, w, ch = rgb_image.shape
            bytes_per_line = ch * w
            convert_to_Qt_format = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)
            p = convert_to_Qt_format
            self.change_pixmap_signal.emit(p)

    def stop(self):
        self.running = False
        if self.cap is not None:
            self.cap.release()
            self.cap = None
        self.quit()
        self.wait(100)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setWindowIcon(QIcon('icons/video_recording.png'))
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setWindowTitle("Крысоход")
        MainWindow.setStyleSheet("background-color: rgb(91, 91, 91);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.image_label = QtWidgets.QLabel(self.centralwidget)
        self.image_label.setGeometry(QtCore.QRect(80, 20, 640, 480))
        self.image_label.setObjectName("image_label")

        self.startButton = QtWidgets.QPushButton(self.centralwidget, text="Начать запись")
        self.startButton.setGeometry(QtCore.QRect(100, 520, 150, 50))
        self.startButton.setStyleSheet("QPushButton {\n"
                                      "    /* Обычное состояние */\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "    background-color: rgb(255, 130, 20);\n"
                                      "    border: 2px solid rgbrgb(255, 130, 20);\n"
                                      "    border-top-color: rgb(255, 255, 255);\n"
                                      "    border-left-color: rgb(255, 255, 255);\n"
                                      "    border-radius: 9px;\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    /* Состояние при нажатии */\n"
                                      "    color: rgb(255, 255, 255);    \n"
                                      "    background-color: rgb(255, 130, 20);\n"
                                      "   border: 2px solid rgb(84, 84, 84);\n"
                                      "    border-top-color: rgb(84, 84, 84);\n"
                                      "    border-left-color: rgb(84, 84, 84);\n"
                                      "    border-radius: 9px;\n"
                                      "}\n"
                                      "\n"
                                      "")

        self.stopButton = QtWidgets.QPushButton(self.centralwidget, text="Остановить запись")
        self.stopButton.setGeometry(QtCore.QRect(550, 520, 150, 50))
        self.stopButton.setStyleSheet("QPushButton {\n"
                                       "    /* Обычное состояние */\n"
                                       "    color: rgb(255, 255, 255);\n"
                                       "    background-color: rgb(255, 130, 20);\n"
                                       "    border: 2px solid rgbrgb(255, 130, 20);\n"
                                       "    border-top-color: rgb(255, 255, 255);\n"
                                       "    border-left-color: rgb(255, 255, 255);\n"
                                       "    border-radius: 9px;\n"
                                       "}\n"
                                       "QPushButton:pressed {\n"
                                       "    /* Состояние при нажатии */\n"
                                       "    color: rgb(255, 255, 255);    \n"
                                       "    background-color: rgb(255, 130, 20);\n"
                                       "   border: 2px solid rgb(84, 84, 84);\n"
                                       "    border-top-color: rgb(84, 84, 84);\n"
                                       "    border-left-color: rgb(84, 84, 84);\n"
                                       "    border-radius: 9px;\n"
                                       "}\n"
                                       "\n"
                                       "")

        # Добавляем индикатор записи
        self.recording_indicator = QtWidgets.QLabel(self.centralwidget)
        self.recording_indicator.setGeometry(QtCore.QRect(750, 20, 30, 30))
        self.recording_indicator.setStyleSheet("background-color: red; border-radius: 15px;")
        self.recording_indicator.setObjectName("recording_indicator")

        MainWindow.setCentralWidget(self.centralwidget)

        self.startButton.clicked.connect(self.start_recording)
        self.stopButton.clicked.connect(self.stop_recording)

        # Настройка видеопотока
        self.thread = VideoThread()
        self.thread.change_pixmap_signal.connect(self.update_image)
        self.thread.start()

        self.video_writer = None
        self.recording = False

        # Таймер для мигания индикатора записи
        self.blink_timer = QTimer()
        self.blink_timer.timeout.connect(self.blink_indicator)
        self.is_blinking = False

    def start_recording(self):
        if self.recording:
            return
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        if not os.path.exists('data/video'):
            os.makedirs('data/video')
        self.video_writer = cv2.VideoWriter('data/video/output.avi', fourcc, 29.0, (640, 480))
        if not self.video_writer.isOpened():
            print("Video writer could not be opened")
            return
        self.recording = True
        # Подключить камеру к видеорекордеру
        self.thread.change_pixmap_signal.connect(self.record_frame)
        # Запуск таймера для мигания индикатора
        self.blink_timer.start(500)

    def stop_recording(self):
        if not self.recording:
            return
        self.recording = False
        if self.video_writer:
            self.thread.change_pixmap_signal.disconnect(self.record_frame)
            self.video_writer.release()
            self.video_writer = None
        # Останавливаем таймер мигания
        self.blink_timer.stop()
        # Возвращаем индикатор в исходное состояние
        self.recording_indicator.setStyleSheet("background-color: red; border-radius: 15px;")
        self.is_blinking = False

        # Останавливаем поток
        self.thread.stop()
        self.thread.wait()

        # Открываем новое окно
        self.open_loading_window()

    def update_image(self, cv_img):
        qt_img = QPixmap.fromImage(cv_img)
        self.image_label.setPixmap(qt_img)

    def record_frame(self, image):
        if self.video_writer:
            # Преобразуем QImage в массив numpy
            image = image.convertToFormat(QImage.Format_RGB888)
            width = image.width()
            height = image.height()
            ptr = image.bits()
            ptr.setsize(image.byteCount())
            arr = np.array(ptr).reshape(height, width, 3)
            frame = cv2.cvtColor(arr, cv2.COLOR_RGB2BGR)  # Переводим из RGB в BGR для OpenCV
            # Пишем кадр в файл
            self.video_writer.write(frame)

    def blink_indicator(self):
        if self.is_blinking:
            self.recording_indicator.setStyleSheet("background-color: red; border-radius: 15px;")
        else:
            self.recording_indicator.setStyleSheet("background-color: green; border-radius: 15px;")
        self.is_blinking = not self.is_blinking

    def open_loading_window(self):
        # Предполагается, что Window - это класс вашего основного окна из main_menu.py
        from main_window import Ui_MainWindow
        self.main_window = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.main_window)
        self.main_window.show()
        self.centralwidget.window().close()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
