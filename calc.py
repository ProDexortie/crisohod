
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(459, 605)
        font = QtGui.QFont()
        font.setPointSize(48)
        MainWindow.setFont(font)
        MainWindow.setLayoutDirection(QtCore.Qt.RightToLeft)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("background-color: rgb(0, 0, 0);\n"
"")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setEnabled(True)
        self.label.setGeometry(QtCore.QRect(10, 10, 441, 91))
        font = QtGui.QFont()
        font.setFamily("Magneto")
        font.setPointSize(48)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setStyleSheet("background-color: rgb(255, 130, 20);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 25px;")
        self.label.setText("")
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(10, 110, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton.setFont(font)
        self.pushButton.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton.setObjectName("pushButton")
        self.pushButton_12 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_12.setGeometry(QtCore.QRect(10, 470, 231, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_12.setFont(font)
        self.pushButton_12.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_12.setObjectName("pushButton_12")
        self.pushButton_13 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_13.setGeometry(QtCore.QRect(370, 110, 75, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_13.setFont(font)
        self.pushButton_13.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_13.setObjectName("pushButton_13")
        self.pushButton_17 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_17.setGeometry(QtCore.QRect(250, 470, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_17.setFont(font)
        self.pushButton_17.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_17.setObjectName("pushButton_17")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(130, 110, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_2.setFont(font)
        self.pushButton_2.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(250, 110, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_3.setFont(font)
        self.pushButton_3.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(250, 230, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_4.setFont(font)
        self.pushButton_4.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(10, 230, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_5.setFont(font)
        self.pushButton_5.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_6 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_6.setGeometry(QtCore.QRect(130, 230, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_6.setFont(font)
        self.pushButton_6.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_6.setObjectName("pushButton_6")
        self.pushButton_7 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_7.setGeometry(QtCore.QRect(130, 350, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_7.setFont(font)
        self.pushButton_7.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_7.setObjectName("pushButton_7")
        self.pushButton_8 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_8.setGeometry(QtCore.QRect(10, 350, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_8.setFont(font)
        self.pushButton_8.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_8.setObjectName("pushButton_8")
        self.pushButton_9 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_9.setGeometry(QtCore.QRect(250, 350, 111, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_9.setFont(font)
        self.pushButton_9.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_9.setObjectName("pushButton_9")
        self.pushButton_14 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_14.setGeometry(QtCore.QRect(370, 230, 75, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_14.setFont(font)
        self.pushButton_14.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_14.setObjectName("pushButton_14")
        self.pushButton_15 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_15.setGeometry(QtCore.QRect(370, 350, 75, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_15.setFont(font)
        self.pushButton_15.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_15.setObjectName("pushButton_15")
        self.pushButton_16 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_16.setGeometry(QtCore.QRect(370, 470, 75, 111))
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_16.setFont(font)
        self.pushButton_16.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgb(255, 130, 20);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid #8F8F8F;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"\n"
"")
        self.pushButton_16.setObjectName("pushButton_16")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.pushButton, self.pushButton_12)

        self.add_functions()

        self.is_equal = False

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Калькулятор"))
        self.pushButton.setText(_translate("MainWindow", "7"))
        self.pushButton_12.setText(_translate("MainWindow", "0"))
        self.pushButton_13.setText(_translate("MainWindow", "-"))
        self.pushButton_17.setText(_translate("MainWindow", "="))
        self.pushButton_2.setText(_translate("MainWindow", "8"))
        self.pushButton_3.setText(_translate("MainWindow", "9"))
        self.pushButton_4.setText(_translate("MainWindow", "6"))
        self.pushButton_5.setText(_translate("MainWindow", "4"))
        self.pushButton_6.setText(_translate("MainWindow", "5"))
        self.pushButton_7.setText(_translate("MainWindow", "2"))
        self.pushButton_8.setText(_translate("MainWindow", "1"))
        self.pushButton_9.setText(_translate("MainWindow", "3"))
        self.pushButton_14.setText(_translate("MainWindow", "+"))
        self.pushButton_15.setText(_translate("MainWindow", "/"))
        self.pushButton_16.setText(_translate("MainWindow", "*"))

    def add_functions(self):
        self.pushButton.clicked.connect(lambda: self.write_number(self.pushButton.text()))
        self.pushButton_12.clicked.connect(lambda: self.write_number(self.pushButton_12.text()))
        self.pushButton_13.clicked.connect(lambda: self.write_number(self.pushButton_13.text()))
        self.pushButton_2.clicked.connect(lambda: self.write_number(self.pushButton_2.text()))
        self.pushButton_3.clicked.connect(lambda: self.write_number(self.pushButton_3.text()))
        self.pushButton_4.clicked.connect(lambda: self.write_number(self.pushButton_4.text()))
        self.pushButton_5.clicked.connect(lambda: self.write_number(self.pushButton_5.text()))
        self.pushButton_6.clicked.connect(lambda: self.write_number(self.pushButton_6.text()))
        self.pushButton_7.clicked.connect(lambda: self.write_number(self.pushButton_7.text()))
        self.pushButton_8.clicked.connect(lambda: self.write_number(self.pushButton_8.text()))
        self.pushButton_9.clicked.connect(lambda: self.write_number(self.pushButton_9.text()))
        self.pushButton_14.clicked.connect(lambda: self.write_number(self.pushButton_14.text()))
        self.pushButton_15.clicked.connect(lambda: self.write_number(self.pushButton_15.text()))
        self.pushButton_16.clicked.connect(lambda: self.write_number(self.pushButton_16.text()))

        self.pushButton_17.clicked.connect(self.resoult)



    def write_number(self, number):
        if self.label.text() == "0" or self.is_equal == True:
            self.label.setText(number)
            self.is_equal = False
        else:
            self.label.setText(self.label.text() + number)

    def resoult(self):
        if not self.is_equal:
            res = eval(self.label.text())
            self.label.setText(str(res))
            self.is_equal = True
        else:
            error = QMessageBox()
            error.setWindowTitle("Error")
            error.setText("Вы не ввели уравнение")
            error.setIcon(QMessageBox.Warning)
            error.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
            error.setDefaultButton(QMessageBox.Ok)
            error.setInformativeText("Da")
            error.setDetailedText("Детали")

            error.buttonClicked.connect(self.popup_action)

            error.exec_()


    def popup_action(self, btn):
        if btn.text() == "Ok":
            print("Print ok")
        elif btn.text() == "Cancel":
            self.label.setText(" ")
            self.is_equal  = False




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


