from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtCore import QUrl, QSize
import os
from PyQt5.QtGui import QIcon
import sys
from PyQt5.QtGui import QPainter, QColor, QPixmap
from PyQt5.QtCore import Qt
import os
from PyQt5.QtWidgets import QApplication


from all_data import (video_folder_path, paw_data_path, lb_photos_path, lf_photos_path, rb_photos_path, rf_photos_path,
                      duration_of_click_path, image_horizontal_lb_width, photo_of_corridor_path)




from all_data import main_folder_path
os.chdir(main_folder_path)


def find_video_file(directory):
    """Поиск первого видеофайла в указанной директории."""
    for file in os.listdir(directory):
        if file.endswith(('.mp4', '.avi', '.mkv')):  # Добавьте нужные форматы файлов
            return os.path.join(directory, file)
    return None



class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1071, 825)
        MainWindow.setWindowIcon(QIcon('icons/main_menu.png'))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(1071, 825))
        MainWindow.setMaximumSize(QtCore.QSize(1071, 825))
        MainWindow.setStyleSheet("background-color: rgb(91, 91, 91);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 1081, 291))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        font.setKerning(True)
        self.tabWidget.setFont(font)
        self.tabWidget.setStyleSheet("QTabWidget::pane { /* Удаление границ вокруг содержимого вкладок */\n"
"    border: 0;\n"
"}\n"
"\n"
"QTabBar::tab { /* Стилизация вкладок */\n"
"    background: rgb(255, 130, 20);\n"
"    color: rgb(255, 255, 255);\n"
"    border: 1px solid rgb(0, 0, 0);\n"
"    padding: 5px;\n"
"    border-radius: 7px;\n"
"}\n"
"\n"
"QTabBar::tab:selected { /* Стилизация выбранной вкладки */\n"
"    background: #ffffff;\n"
"    color: rgb(0, 0, 0);\n"
"    border-color: #ffffff;\n"
"    border-radius:1px;\n"
"\n"
"}\n"
"")
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.hallway_photo = QtWidgets.QLabel(self.tab)
        self.hallway_photo.setGeometry(QtCore.QRect(0, 0, 1081, 131))
        self.hallway_photo.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.hallway_photo.setText("")
        self.hallway_photo.setObjectName("hallway_photo")

        photo_of_corridor = QPixmap(photo_of_corridor_path)
        scaled_photo_of_corridor = photo_of_corridor.scaled(self.hallway_photo.width(),
                                                            self.hallway_photo.height(), Qt.KeepAspectRatio)

        self.hallway_photo.setPixmap(scaled_photo_of_corridor)

        self.verticalLayoutWidget = QtWidgets.QWidget(self.tab)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(170, 140, 901, 111))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.verticalLayoutWidget.setFont(font)
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.image_horizontal_lf = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.image_horizontal_lf.setFont(font)
        self.image_horizontal_lf.setStyleSheet("background-color: rgb(135, 135, 135);\n"
"")
        self.image_horizontal_lf.setText("")
        self.image_horizontal_lf.setObjectName("image_horizontal_lf")
        self.verticalLayout.addWidget(self.image_horizontal_lf)
        self.image_horizontal_rf = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.image_horizontal_rf.setFont(font)
        self.image_horizontal_rf.setStyleSheet("background-color: rgb(206, 206, 206);")
        self.image_horizontal_rf.setText("")
        self.image_horizontal_rf.setObjectName("image_horizontal_rf")
        self.verticalLayout.addWidget(self.image_horizontal_rf)
        self.image_horizontal_lb = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.image_horizontal_lb.setFont(font)
        self.image_horizontal_lb.setStyleSheet("background-color: rgb(135, 135, 135);")
        self.image_horizontal_lb.setText("")
        self.image_horizontal_lb.setObjectName("image_horizontal_lb")
        self.verticalLayout.addWidget(self.image_horizontal_lb)
        self.image_horizontal_rb = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.image_horizontal_rb.setFont(font)
        self.image_horizontal_rb.setStyleSheet("background-color: rgb(206, 206, 206);")
        self.image_horizontal_rb.setText("")
        self.image_horizontal_rb.setObjectName("image_horizontal_rb")
        self.verticalLayout.addWidget(self.image_horizontal_rb)
        self.verticalLayoutWidget_2 = QtWidgets.QWidget(self.tab)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(0, 140, 170, 111))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.verticalLayoutWidget_2.setFont(font)
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontal_lf = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.horizontal_lf.setFont(font)
        self.horizontal_lf.setStyleSheet("background-color: rgb(135, 135, 135);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"QLabel {\n"
"    color: white;\n"
"    background-color: black;\n"
"    padding: 1px; /* Минимальное расстояние текста от краев виджета */\n"
"}\n"
"")
        self.horizontal_lf.setObjectName("horizontal_lf")
        self.verticalLayout_2.addWidget(self.horizontal_lf)
        self.horizontal_rf = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.horizontal_rf.setFont(font)
        self.horizontal_rf.setStyleSheet("background-color: rgb(206, 206, 206);")
        self.horizontal_rf.setObjectName("horizontal_rf")
        self.verticalLayout_2.addWidget(self.horizontal_rf)
        self.horizontal_lb = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.horizontal_lb.setFont(font)
        self.horizontal_lb.setStyleSheet("background-color: rgb(135, 135, 135);\n"
"color: rgb(255, 255, 255);\n"
"")
        self.horizontal_lb.setObjectName("horizontal_lb")
        self.verticalLayout_2.addWidget(self.horizontal_lb)
        self.horizontal_rb = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.horizontal_rb.setFont(font)
        self.horizontal_rb.setStyleSheet("background-color: rgb(206, 206, 206);")
        self.horizontal_rb.setObjectName("horizontal_rb")
        self.verticalLayout_2.addWidget(self.horizontal_rb)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.horizontalSlider = QtWidgets.QSlider(self.tab_2)
        self.horizontalSlider.setGeometry(QtCore.QRect(0, 230, 1071, 31))
        self.horizontalSlider.setStyleSheet("QSlider::groove:horizontal {\n"
"    border: 1px solid #999999; /* Цвет границы дорожки */\n"
"    height: 9px; /* Высота дорожки */\n"
"    background: #e0e0e0; /* Цвет фона дорожки */\n"
"    margin: 2px 0; /* Отступы вокруг дорожки */\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: rgb(255, 130, 20); /* Цвет ползунка */\n"
"    border: 1px solid #5c5c5c; /* Цвет границы ползунка */\n"
"    width: 12px; /* Ширина ползунка */\n"
"    margin: -28px 0; /* Вертикальный отступ ползунка от дорожки */\n"
"    border-radius: 20px; /* Закругление углов ползунка */\n"
"}\n"
"\n"
"QSlider::sub-page:horizontal {\n"
"    background: rgb(0, 161, 0); /* Зеленый цвет для заполненной части слайдера */\n"
"}\n"
"\n"
"QSlider::add-page:horizontal {\n"
"    background: #d3d3d3; /* Серый цвет для незаполненной части слайдера */\n"
"}\n"
"")
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setObjectName("horizontalSlider")
        self.horizontalSlider.sliderMoved.connect(self.set_media_position)


        self.videoWiget = QVideoWidget(self.tab_2)
        self.videoWiget.setGeometry(QtCore.QRect(0, 0, 1071, 171))
        self.videoWiget.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.videoWiget.setObjectName("videoWiget")

        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        self.mediaPlayer.setVideoOutput(self.videoWiget)

        video_path = find_video_file(video_folder_path)

        if video_path:
                self.mediaPlayer.setMedia(QMediaContent(QUrl.fromLocalFile(video_path)))
        else:
                print("Видеофайл не найден.")

        if video_path:
                self.mediaPlayer.setMedia(QMediaContent(QUrl.fromLocalFile(video_path)))
        else:
                print("Видеофайл не найден.")


        self.mediaPlayer.durationChanged.connect(self.media_duration_changed)
        self.mediaPlayer.positionChanged.connect(self.media_position_changed)


        self.playButton = QtWidgets.QPushButton(self.tab_2)
        self.playButton.setGeometry(QtCore.QRect(450, 180, 81, 41))
        self.playButton.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgbrgb(255, 130, 20);\n"
"    border-top-color: rgb(255, 255, 255);\n"
"    border-left-color: rgb(255, 255, 255);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    color: rgb(255, 255, 255);    \n"
"    background-color: rgb(255, 130, 20);\n"
"   border: 2px solid rgb(84, 84, 84);\n"
"    border-top-color: rgb(84, 84, 84);\n"
"    border-left-color: rgb(84, 84, 84);\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"")
        self.playButton.setText("")
        self.playButton.setObjectName("playButton")
        self.playButton.setIcon(QIcon(main_folder_path + r"/icons/play.png"))
        self.playButton.setIconSize(QSize(35, 35))
        self.playButton.clicked.connect(self.mediaPlayer.play)

        self.pauseButton = QtWidgets.QPushButton(self.tab_2)
        self.pauseButton.setGeometry(QtCore.QRect(540, 180, 81, 41))
        self.pauseButton.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgbrgb(255, 130, 20);\n"
"    border-top-color: rgb(255, 255, 255);\n"
"    border-left-color: rgb(255, 255, 255);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    color: rgb(255, 255, 255);    \n"
"    background-color: rgb(255, 130, 20);\n"
"   border: 2px solid rgb(84, 84, 84);\n"
"    border-top-color: rgb(84, 84, 84);\n"
"    border-left-color: rgb(84, 84, 84);\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"")
        self.pauseButton.setText("")
        self.pauseButton.setObjectName("pauseButton")
        self.pauseButton.setIcon(QIcon(main_folder_path + r"/icons/pause.png"))
        self.pauseButton.setIconSize(QSize(35, 35))
        self.pauseButton.clicked.connect(self.mediaPlayer.pause)
        self.videoBeginning = QtWidgets.QPushButton(self.tab_2)
        self.videoBeginning.setGeometry(QtCore.QRect(360, 180, 81, 41))
        self.videoBeginning.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgbrgb(255, 130, 20);\n"
"    border-top-color: rgb(255, 255, 255);\n"
"    border-left-color: rgb(255, 255, 255);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    color: rgb(255, 255, 255);    \n"
"    background-color: rgb(255, 130, 20);\n"
"   border: 2px solid rgb(84, 84, 84);\n"
"    border-top-color: rgb(84, 84, 84);\n"
"    border-left-color: rgb(84, 84, 84);\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"")
        self.videoBeginning.setText("")
        self.videoBeginning.setObjectName("videoBeginning")
        self.videoBeginning.setIcon(QIcon(main_folder_path + r"/icons/step_left.png"))
        self.videoBeginning.setIconSize(QSize(35, 35))
        self.videoBeginning.clicked.connect(self.seek_to_start)
        self.videoEnd = QtWidgets.QPushButton(self.tab_2)
        self.videoEnd.setGeometry(QtCore.QRect(630, 180, 81, 41))
        self.videoEnd.setStyleSheet("QPushButton {\n"
"    /* Обычное состояние */\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(255, 130, 20);\n"
"    border: 2px solid rgbrgb(255, 130, 20);\n"
"    border-top-color: rgb(255, 255, 255);\n"
"    border-left-color: rgb(255, 255, 255);\n"
"    border-radius: 9px;\n"
"}\n"
"QPushButton:pressed {\n"
"    /* Состояние при нажатии */\n"
"    color: rgb(255, 255, 255);    \n"
"    background-color: rgb(255, 130, 20);\n"
"   border: 2px solid rgb(84, 84, 84);\n"
"    border-top-color: rgb(84, 84, 84);\n"
"    border-left-color: rgb(84, 84, 84);\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"")
        self.videoEnd.setText("")
        self.videoEnd.setObjectName("videoEnd")
        self.videoEnd.setIcon(QIcon(main_folder_path + r"/icons/step_right.png"))
        self.videoEnd.setIconSize(QSize(35, 35))
        self.videoEnd.clicked.connect(self.seek_to_end)
        self.tabWidget.addTab(self.tab_2, "")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(10, 300, 551, 80))
        self.widget.setStyleSheet("\n"
"border-radius: 9px;\n"
"background-color: rgb(113, 113, 113)")
        self.widget.setObjectName("widget")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.widget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 0, 531, 80))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.checkBox_lf = QtWidgets.QCheckBox(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.checkBox_lf.setFont(font)
        self.checkBox_lf.setStyleSheet("color: rgb(255, 255, 255);")
        self.checkBox_lf.setObjectName("checkBox_lf")
        self.horizontalLayout.addWidget(self.checkBox_lf)
        self.checkBox_rf = QtWidgets.QCheckBox(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.checkBox_rf.setFont(font)
        self.checkBox_rf.setStyleSheet("color: rgb(255, 255, 255);")
        self.checkBox_rf.setObjectName("checkBox_rf")
        self.horizontalLayout.addWidget(self.checkBox_rf)
        self.checkBox_lb = QtWidgets.QCheckBox(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.checkBox_lb.setFont(font)
        self.checkBox_lb.setStyleSheet("color: rgb(255, 255, 255);")
        self.checkBox_lb.setObjectName("checkBox_lb")
        self.horizontalLayout.addWidget(self.checkBox_lb)
        self.checkBox_rb = QtWidgets.QCheckBox(self.horizontalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.checkBox_rb.setFont(font)
        self.checkBox_rb.setStyleSheet("color: rgb(255, 255, 255);")
        self.checkBox_rb.setObjectName("checkBox_rb")
        self.horizontalLayout.addWidget(self.checkBox_rb)



        # Настройка checkBox сигналов
        self.checkBox_lf.stateChanged.connect(self.update_list_widget)
        self.checkBox_rf.stateChanged.connect(self.update_list_widget)
        self.checkBox_lb.stateChanged.connect(self.update_list_widget)
        self.checkBox_rb.stateChanged.connect(self.update_list_widget)


        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(10, 400, 551, 171))
        self.listWidget.setStyleSheet("border-radius: 9px;\n"
"background-color: rgb(0, 0, 0);\n"
"border: 1px solid rgb(255, 130, 20);")
        self.listWidget.setFlow(QtWidgets.QListView.LeftToRight)
        self.listWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.listWidget.setObjectName("listWidget")

        # Связывание события выбора элемента с функцией отображения изображения
        self.listWidget.itemClicked.connect(self.displayImage)

        # Подгрузка данных из файла
        self.pawData = self.loadPawData(paw_data_path)

        # Подключение сигнала к слоту для вывода аннотации
        self.listWidget.itemClicked.connect(self.displayPawInfo)



        self.widget_3 = QtWidgets.QWidget(self.centralwidget)
        self.widget_3.setGeometry(QtCore.QRect(580, 300, 481, 481))
        self.widget_3.setStyleSheet("\n"
"border-radius: 9px;\n"
"background-color: rgb(113, 113, 113)")
        self.widget_3.setObjectName("widget_3")
        self.label_full_image = QtWidgets.QLabel(self.widget_3)
        self.label_full_image.setGeometry(QtCore.QRect(10, 10, 461, 461))
        self.label_full_image.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_full_image.setText("")
        self.label_full_image.setObjectName("label_full_image")
        self.widget_4 = QtWidgets.QWidget(self.centralwidget)
        self.widget_4.setGeometry(QtCore.QRect(10, 590, 551, 191))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_4.sizePolicy().hasHeightForWidth())
        self.widget_4.setSizePolicy(sizePolicy)
        self.widget_4.setStyleSheet("\n"
"border-radius: 9px;\n"
"background-color: rgb(113, 113, 113)")
        self.widget_4.setObjectName("widget_4")
        self.textBrowser = QtWidgets.QTextBrowser(self.widget_4)
        self.textBrowser.setGeometry(QtCore.QRect(10, 10, 531, 171))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.textBrowser.setFont(font)
        self.textBrowser.setStyleSheet("background-color: rgb(211, 211, 211);\n"
"color: rgb(0, 0, 0);")
        self.textBrowser.setObjectName("textBrowser")
        self.widget_3.raise_()
        self.tabWidget.raise_()
        self.widget.raise_()
        self.listWidget.raise_()
        self.widget_4.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1071, 23))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.menubar.setFont(font)
        self.menubar.setStyleSheet("border: 1px solid #ffffff;\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(240, 240, 240);")
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        self.menu_2 = QtWidgets.QMenu(self.menubar)
        self.menu_2.setObjectName("menu_2")
        self.menu_3 = QtWidgets.QMenu(self.menubar)
        self.menu_3.setObjectName("menu_3")
        MainWindow.setMenuBar(self.menubar)
        self.action_5 = QtWidgets.QAction(MainWindow)
        self.action_5.setObjectName("action_5")
        self.action_6 = QtWidgets.QAction(MainWindow)
        self.action_6.setObjectName("action_6")
        self.action_7 = QtWidgets.QAction(MainWindow)
        self.action_7.setObjectName("action_7")
        self.action_8 = QtWidgets.QAction(MainWindow)
        self.action_8.setObjectName("action_8")
        self.action_9 = QtWidgets.QAction(MainWindow)
        self.action_9.setObjectName("action_9")
        self.action_11 = QtWidgets.QAction(MainWindow)
        self.action_11.setObjectName("action_11")
        self.menu.addAction(self.action_5)
        self.menu.addAction(self.action_6)
        self.menu_2.addAction(self.action_8)
        self.menu_2.addAction(self.action_9)
        self.menu_2.addSeparator()
        self.menu_2.addAction(self.action_7)
        self.menu_2.addAction(self.action_11)
        self.menubar.addAction(self.menu_2.menuAction())
        self.menubar.addAction(self.menu.menuAction())
        self.menubar.addAction(self.menu_3.menuAction())

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.prepare_data_visualization()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.horizontal_lf.setText(_translate("MainWindow", "    Левая передняя"))
        self.horizontal_rf.setText(_translate("MainWindow", "    Правая передняя"))
        self.horizontal_lb.setText(_translate("MainWindow", "    Левая задняя"))
        self.horizontal_rb.setText(_translate("MainWindow", "    Правая задняя"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "Результаты"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "Видео"))
        self.checkBox_lf.setText(_translate("MainWindow", "Левая передняя"))
        self.checkBox_rf.setText(_translate("MainWindow", "Правая передняя"))
        self.checkBox_lb.setText(_translate("MainWindow", "Левая задняя"))
        self.checkBox_rb.setText(_translate("MainWindow", "Правая задняя"))
        self.textBrowser.setHtml(_translate("MainWindow", "Выберите фото"))
        self.menu.setTitle(_translate("MainWindow", "Настройки"))
        self.menu_2.setTitle(_translate("MainWindow", "Файл"))
        self.menu_3.setTitle(_translate("MainWindow", "Помощь"))
        self.action_5.setText(_translate("MainWindow", "Изменить пароль"))
        self.action_6.setText(_translate("MainWindow", "Изменить камеру"))
        self.action_7.setText(_translate("MainWindow", "Сохранить файл"))
        self.action_8.setText(_translate("MainWindow", "Нойвое видео"))
        self.action_9.setText(_translate("MainWindow", "Открыть файл"))
        self.action_11.setText(_translate("MainWindow", "Сохранить как..."))
        self.mediaPlayer.error.connect(self.handle_media_error)

        self.action_8.triggered.connect(self.open_recording_windov)
        self.action_9.triggered.connect(self.open_recording_windov)

    def open_recording_windov(self):
        from selection_window import Ui_MainWindow  # Импорт класса из test.py
        self.main_window = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.main_window)
        self.main_window.show()
        self.centralwidget.window().close()


    def calculate_max_duration(self, data):
        max_duration = 0
        for line in data.strip().split('\n'):
            if line.strip():
                intervals_str = line.split(':', 1)[1].strip().strip('{}')
                for interval in intervals_str.split(';'):
                    _, end = interval.split('-')
                    end_sec = self.parse_time(end.strip())
                    if end_sec > max_duration:
                        max_duration = end_sec
        return max_duration

    def prepare_data_visualization(self):
        labels = {
            'lf': self.image_horizontal_lf,
            'rf': self.image_horizontal_rf,
            'lb': self.image_horizontal_lb,
            'rb': self.image_horizontal_rb
        }
        data = self.read_contact_data(duration_of_click_path)
        max_duration = self.calculate_max_duration(data)
        self.update_labels_with_data(labels, data, max_duration)

    def read_contact_data(self, filepath):
        try:
            with open(filepath, 'r') as file:
                return file.read()
        except Exception as e:
            print(f"Ошибка чтения файла: {e}")
            return ""

    def parse_time(self, time_str):
        # Разбор времени, убедитесь, что удаляются только числовые значения
        time_parts = time_str.split(':')
        h, m, s = map(int, [time_part.strip() for time_part in time_parts])
        return h * 3600 + m * 60 + s

    def parse_intervals(self, intervals_str):
        intervals = []
        # Удаление фигурных скобок и лишних пробелов перед разделением на интервалы
        for interval in intervals_str.strip('{} ').split(';'):
            start, end = interval.strip().split('-')
            start_sec = self.parse_time(start.strip())
            end_sec = self.parse_time(end.strip())
            intervals.append((start_sec, end_sec))
        return intervals

    def update_labels_with_data(self, labels, data, total_duration):
        for label in labels.values():
            label.clear()
        for line in data.strip().split('\n'):
            if line.strip():
                paw, intervals_str = line.split(':', 1)
                intervals = self.parse_intervals(intervals_str.strip('{}').strip())
                self.draw_intervals(labels[paw.strip()], intervals, total_duration)

    def draw_intervals(self, label, intervals, total_duration):
        pixmap_width = image_horizontal_lb_width # Заданная ширина
        pixmap_height = label.height()  # Текущая высота QLabel
        pixmap = QPixmap(pixmap_width, pixmap_height)
        pixmap.fill(Qt.white)  # Заполняем фон белым цветом
        painter = QPainter(pixmap)
        colors = {'lf': Qt.red, 'rf': Qt.blue, 'lb': Qt.green, 'rb': Qt.yellow}

        scale = pixmap_width / total_duration  # Масштабируем в соответствии с total_duration

        for start, end in intervals:
            color = colors[label.objectName()[
                           -2:]]  # Получаем имя лапы из названия QLabel, например, 'lb' из 'image_horizontal_lb'
            painter.setBrush(color)
            rect_start = int(start * scale)
            rect_width = int((end - start) * scale)
            painter.drawRect(rect_start, 0, rect_width, pixmap_height)

        painter.end()
        label.setPixmap(pixmap)  # Устанавливаем созданный pixmap в QLabel

    def handle_media_error(self):
            error = self.mediaPlayer.error()
            error_string = self.mediaPlayer.errorString()
            print("Media Player Error:", error)
            print("Error Details:", error_string)

    def update_list_widget(self):
            self.listWidget.clear()  # Очистка предыдущих элементов
            folders = {
                    lf_photos_path: self.checkBox_lf.isChecked(),
                    rf_photos_path: self.checkBox_rf.isChecked(),
                    lb_photos_path: self.checkBox_lb.isChecked(),
                    rb_photos_path: self.checkBox_rb.isChecked()
            }
            for folder, checked in folders.items():
                    if checked:
                            self.add_images_from_folder(folder)

    def add_images_from_folder(self, folder_path):
            valid_extensions = ['.jpg', '.jpeg', '.png', '.bmp', '.gif']
            self.listWidget.setIconSize(QtCore.QSize(126, 126))
            try:
                    for filename in os.listdir(folder_path):
                            if any(filename.endswith(ext) for ext in valid_extensions):
                                    file_path = os.path.join(folder_path, filename)
                                    icon = QtGui.QIcon(file_path)
                                    base_name = os.path.splitext(filename)[0]
                                    item = QtWidgets.QListWidgetItem(icon, base_name)
                                    self.listWidget.addItem(item)
            except Exception as e:
                    self.textBrowser.setText("Error accessing folder:", folder_path, "->", str(e))

    def displayImage(self, item):
            icon = item.icon()
            pixmap = icon.pixmap(icon.availableSizes()[0])
            self.label_full_image.setPixmap(pixmap)

    def loadPawData(self, file_path):
            paw_data = {}
            with open(file_path, "r", encoding="utf-8") as file:
                    for line in file:
                            if ':' in line:
                                    key, value = line.split(':', 1)
                                    paw_data[key.strip()] = value.strip()
            return paw_data

    def displayPawInfo(self, item):
            # Получаем базовое имя файла без расширения
            full_path = item.text()  # Если item.text() возвращает полный путь
            base_name = os.path.splitext(os.path.basename(full_path))[0]

            # Ищем информацию по этому имени в словаре
            info = self.pawData.get(base_name, "Ошибка: нет аннотации к фото")
            self.textBrowser.setText(info)

    def seek_to_start(self):
            """Перемещает видео в начало."""
            self.mediaPlayer.setPosition(0)

    def seek_to_end(self):
            """Перемещает видео в конец."""
            if self.mediaPlayer.duration() > 1000:  # Проверяем, достаточно ли длинное видео
                    self.mediaPlayer.setPosition(
                            self.mediaPlayer.duration() - 1000)  # Установка позиции на 1 секунду до конца
            else:
                    self.mediaPlayer.setPosition(
                            self.mediaPlayer.duration())  # Если видео очень короткое, переместить в самый конец

    def media_duration_changed(self, duration):
            """Обновляет максимальное значение слайдера при изменении длительности медиа."""
            self.horizontalSlider.setRange(0, duration)

    def media_position_changed(self, position):
            """Обновляет положение слайдера при изменении позиции медиа."""
            self.horizontalSlider.setValue(position)

    def set_media_position(self, position):
            """Устанавливает позицию медиаплеера при перемещении слайдера пользователем."""
            self.mediaPlayer.setPosition(position)



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
