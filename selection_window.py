from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox
import shutil
import os
from PyQt5.QtGui import QIcon, QFont

from all_data import main_folder_path, video_folder_path
os.chdir(main_folder_path)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        self.MainWindow = MainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowIcon(QIcon('icons/choosing.png'))
        MainWindow.resize(611, 182)

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(611, 182))
        MainWindow.setMaximumSize(QtCore.QSize(611, 182))
        MainWindow.setStyleSheet("background-color: rgb(91, 91, 91);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(30, 20, 271, 141))
        font = QtGui.QFont()
        font.setPointSize(26)
        self.pushButton.setFont(font)
        self.pushButton.setStyleSheet("QPushButton {\n"
                                      "    /* Обычное состояние */\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "    background-color: rgb(255, 130, 20);\n"
                                      "    border: 2px solid rgb(255, 130, 20);\n"
                                      "    border-top-color: rgb(255, 255, 255);\n"
                                      "    border-left-color: rgb(255, 255, 255);\n"
                                      "    border-radius: 9px;\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    /* Состояние при нажатии */\n"
                                      "    color: rgb(255, 255, 255);\n"
                                      "    background-color: rgb(255, 130, 20);\n"
                                      "    border: 2px solid rgb(84, 84, 84);\n"
                                      "    border-top-color: rgb(84, 84, 84);\n"
                                      "    border-left-color: rgb(84, 84, 84);\n"
                                      "    border-radius: 9px;\n"
                                      "}\n")
        self.pushButton.setObjectName("pushButton")
        font = QFont()
        font.setPointSize(22)
        self.pushButton.setFont(font)

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(310, 20, 271, 141))
        font = QtGui.QFont()
        font.setPointSize(25)
        self.pushButton_2.setFont(font)
        self.pushButton_2.setStyleSheet("QPushButton {\n"
                                        "    /* Обычное состояние */\n"
                                        "    color: rgb(255, 255, 255);\n"
                                        "    background-color: rgb(255, 130, 20);\n"
                                        "    border: 2px solid rgb(255, 130, 20);\n"
                                        "    border-top-color: rgb(255, 255, 255);\n"
                                        "    border-left-color: rgb(255, 255, 255);\n"
                                        "    border-radius: 9px;\n"
                                        "}\n"
                                        "QPushButton:pressed {\n"
                                        "    /* Состояние при нажатии */\n"
                                        "    color: rgb(255, 255, 255);\n"
                                        "    background-color: rgb(255, 130, 20);\n"
                                        "    border: 2px solid rgb(84, 84, 84);\n"
                                        "    border-top-color: rgb(84, 84, 84);\n"
                                        "    border-left-color: rgb(84, 84, 84);\n"
                                        "    border-radius: 9px;\n"
                                        "}\n")
        self.pushButton_2.setObjectName("pushButton_2")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.pushButton.clicked.connect(self.open_video_file)
        self.pushButton_2.clicked.connect(self.open_recording_window)

    def open_recording_window(self):
        from all_data import video_folder_path, processed_video_folder_path
        from delete_everything_from_folder import delete_files_in_folder
        delete_files_in_folder(video_folder_path)
        delete_files_in_folder(processed_video_folder_path)

        from video_recording_window import Ui_MainWindow
        self.main_window = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.main_window)
        self.main_window.show()
        self.MainWindow.close()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Крысоход"))
        self.pushButton.setText(_translate("MainWindow", "Открыть видео"))
        self.pushButton_2.setText(_translate("MainWindow", "Новая запись"))

    def open_video_file(self):
        from all_data import video_folder_path, processed_video_folder_path
        from delete_everything_from_folder import delete_files_in_folder
        delete_files_in_folder(video_folder_path)
        delete_files_in_folder(processed_video_folder_path)

        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getOpenFileName(None, "Выберите видеофайл", "",
                                                  "Video Files (*.mp4 *.avi *.mov);;All Files (*)", options=options)
        if fileName:
            target_directory = video_folder_path
            if not os.path.exists(target_directory):
                os.makedirs(target_directory)

            target_file = os.path.join(target_directory, os.path.basename(fileName))

            file_suffix = 1
            base_name, ext = os.path.splitext(target_file)
            while os.path.exists(target_file):
                target_file = f"{base_name}({file_suffix}){ext}"
                file_suffix += 1

            try:
                print('ok1')
                shutil.copy(fileName, target_file)
                print('ok2')
                from loading import Ui_MainWindow
                print('ok3')
                self.main_window = QtWidgets.QMainWindow()
                print('ok4')
                self.ui = Ui_MainWindow()
                print('ok5')
                self.ui.setupUi(self.main_window)
                print('ok6')
                self.main_window.show()
                print('ok7')
                self.MainWindow.close()
                print('ok8')
            except Exception as e:
                QMessageBox.critical(None, "Ошибка", f"Не удалось скопировать файл: {str(e)}")

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
