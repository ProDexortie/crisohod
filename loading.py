from PyQt5 import QtCore, QtGui, QtWidgets
from model import model_processing

class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.init_thread()

    def setupUi(self, MainWindow):
        try:
            print("Setting up UI")
            ProgressBarValue = 0
            MainWindow.setObjectName("MainWindow")
            MainWindow.resize(416, 232)
            MainWindow.setStyleSheet("background-color: rgb(91, 91, 91);")
            self.centralwidget = QtWidgets.QWidget(MainWindow)
            self.centralwidget.setObjectName("centralwidget")
            self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
            self.progressBar.setGeometry(QtCore.QRect(50, 110, 311, 101))
            font = QtGui.QFont()
            font.setPointSize(17)
            self.progressBar.setFont(font)
            self.progressBar.setStyleSheet("border-radius: 9px;\n"
                                           "background-color:rgb(255, 130, 20);\n"
                                           "color: rgb(255, 255, 255);\n"
                                           "")
            self.progressBar.setProperty("value", ProgressBarValue)
            self.progressBar.setAlignment(QtCore.Qt.AlignCenter)
            self.progressBar.setTextVisible(True)
            self.progressBar.setOrientation(QtCore.Qt.Horizontal)
            self.progressBar.setInvertedAppearance(False)
            self.progressBar.setObjectName("progressBar")
            self.label = QtWidgets.QLabel(self.centralwidget)
            self.label.setGeometry(QtCore.QRect(90, 10, 241, 91))
            font = QtGui.QFont()
            font.setFamily("MS Reference Sans Serif")
            font.setPointSize(25)
            self.label.setFont(font)
            self.label.setStyleSheet("color: rgb(255, 255, 255);")
            self.label.setObjectName("label")
            MainWindow.setCentralWidget(self.centralwidget)

            self.retranslateUi(MainWindow)
            QtCore.QMetaObject.connectSlotsByName(MainWindow)
        except Exception as e:
            print(f"Error in setupUi: {e}")

    def init_thread(self):
        try:
            print("Creating thread")
            self.thread = QtCore.QThread(self)
            self.worker = Worker()
            self.worker.moveToThread(self.thread)
            self.worker.progress.connect(self.update_progress)
            self.worker.finished.connect(self.on_worker_finished)
            self.thread.started.connect(self.worker.run)
            self.thread.finished.connect(self.on_thread_finished)

            print("Starting thread")
            self.thread.start()
        except Exception as e:
            print(f"Error in init_thread: {e}")

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Подождите"))

    @QtCore.pyqtSlot(float)
    def update_progress(self, progress):
        try:
            print(f"Updating progress: {progress}%")
            self.progressBar.setProperty("value", int(progress))
            if progress >= 100:
                self.worker.finished.emit()
        except Exception as e:
            print(f"Error in update_progress: {e}")

    @QtCore.pyqtSlot()
    def on_thread_finished(self):
        print("Thread finished")

    @QtCore.pyqtSlot()
    def on_worker_finished(self):
        print("Worker finished, opening main_window")
        from main_window import Ui_MainWindow as MainUI
        self.main_window = QtWidgets.QMainWindow()
        self.ui = MainUI()
        self.ui.setupUi(self.main_window)
        self.main_window.show()
        self.close()

class Worker(QtCore.QObject):
    progress = QtCore.pyqtSignal(float)
    finished = QtCore.pyqtSignal()

    def run(self):
        try:
            print("Worker running")
            model_processing(self.report_progress)
        except Exception as e:
            print(f"Error in worker run: {e}")

    def report_progress(self, progress):
        self.progress.emit(progress)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = Ui_MainWindow()
    MainWindow.show()
    sys.exit(app.exec_())
