import os
from all_data import video_folder_path, main_folder_path
os.chdir(main_folder_path)

def delete_files_in_folder(folder):

    # Перебираем все файлы в папке и удаляем их
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                # Если нужно удалить директории, раскомментируйте следующую строку
                # shutil.rmtree(file_path)
                pass
        except Exception as e:
            print(f'Не удалось удалить {file_path}. Причина: {e}')



