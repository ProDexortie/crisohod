import bcrypt

# Пароль, который вы хотите хешировать
password = b"123"

# Генерация хеша
hashed = bcrypt.hashpw(password, bcrypt.gensalt())

# Запись хеша в файл
with open("password_hash.txt", "wb") as f:
    f.write(hashed)
