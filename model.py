import cv2
import os
from ultralytics import YOLO
from all_data import model_path, video_folder_path, processed_video_folder_path, main_folder_path

os.chdir(main_folder_path)


def find_video_file(directory):
    """Поиск первого видеофайла в указанной директории."""
    for file in os.listdir(directory):
        if file.endswith(('.mp4', '.avi', '.mkv')):  # Добавьте нужные форматы файлов
            return os.path.join(directory, file)
    return None


def model_processing(callback=None):
    # Загружаем модель
    model = YOLO(model_path)

    # Путь к видео
    video_path = find_video_file(video_folder_path)

    save_path = processed_video_folder_path

    # Открываем видео для чтения
    cap = cv2.VideoCapture(video_path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # Создаем VideoWriter для записи видео
    output_video_path = os.path.join(save_path, "processed_video.mp4")
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_video_path, fourcc, fps, (frame_width, frame_height))

    frame_idx = 0
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        # Обрабатываем кадр с помощью модели
        results = model.predict(frame)

        # Получаем аннотированный кадр
        annotated_frame = results[0].plot()

        # Записываем аннотированный кадр в выходное видео
        out.write(annotated_frame)

        frame_idx += 1
        # Обновляем прогресс
        progress = (frame_idx / frame_count) * 100

        if callback:
            callback(progress)

    cap.release()
    out.release()

    print(f"Обработанное видео сохранено по пути: {output_video_path}")
